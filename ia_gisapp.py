import json
import logging
import os

from src.conf import config,dbaccess
import geopandas as gpd
import pandas as pd
from shapely.geometry import Point


# logging configuration
logging.basicConfig(level=logging.INFO,format='%(asctime)s -%(levelname)s - %(message)s')

conn = dbaccess.get_ne_pgis_db()
cur = conn.cursor()

boundary_shape = gpd.read_file(config.ia_boundary_file)

# Output keys
keys = ['distance','closest_point','geographic_identifier','system_code',
        'route_number', 'direction','ramp_sequence','route_id']

# For each file in the folder
for file in [file for file in os.listdir(config.ia_input_folder) if file.endswith('.csv')]:
    logging.info('Processing started for file %s.', file)
    file_base_name = os.path.splitext(os.path.basename(file))[0]
    df = pd.read_csv(os.path.join(config.ia_input_folder, file))
    df['gps_lat'].fillna(0,inplace=True)
    df['gps_long'].fillna(0,inplace=True)
    df['is_ia'] = df[['gps_long','gps_lat']].apply(Point,axis=1).apply(lambda p: boundary_shape.contains(p))

    # Group files based on the unique drive (combination of subj and drive)
    dfg = df.groupby(['subj','drive'])

    output_file = os.path.join(config.ne_output_folder, file_base_name + '_processed.csv')

    for key, group in dfg:
        logging.info('Spatial analysis for group %s', key)
        basedf = group[["subj", "file", "drive", "time_utc", "gps_lat", "gps_long", "gps_heading", "is_ne"]]
        result_arr = []
        total = group['is_ia'].sum()
        print('total ',total)
        if total > 1:
           logging.info('Route is in Iowa.')
           for id,row in group.iterrows():
               cur.callproc('blackbox.ia_closest_point_on_line', ('POINT({} {})'.format(row['gps_long'],row['gps_lat']),
                                                               config.ne_tolerance))
               records = cur.fetchall()
               if len(records) > 0:
                   # logging.info('Resolved the nearest route ')
                   # The database function returns at most one record
                   record = dict(zip(keys,records[0]))
                   coordinates = json.loads(record['closest_point'])['coordinates']
                   # Remove key from the dictionary
                   record.pop('closest_point',None)
                   record['route_long'] = coordinates[0]
                   record['route_lat'] = coordinates[1]
                   record['route_50ft'] = 1
                   record['route_success'] = 1
                   result_arr.append(record)
               else :
                   # logging.info('No nearest route exist.')
                   record = {k:'' for k in keys if k != 'closest_point'}
                   record['distance'] = -1
                   record['route_long'] = ''
                   record['route_lat'] = ''
                   record['route_50ft'] = 0
                   record['route_success'] = 1
                   result_arr.append(record)
        else:
           logging.info('Route is not in Nebraska')
           for id,row in group.iterrows():
               record = {k: '' for k in keys if k != 'closest_point'}
               record['distance'] = ''
               record['route_long'] = ''
               record['route_lat'] = ''
               record['route_50ft'] = 0
               record['route_success'] = 0
               result_arr.append(record)

        resultdf = pd.DataFrame(result_arr,index=basedf.index)
        finaldf = basedf.join(resultdf)
        finaldf.to_csv(output_file, index=False, mode='a', header=(not os.path.exists(output_file)))
        logging.info('Finished processing for %s',file)

logging.info('Total processing finished in folder %s', config.ia_input_folder)