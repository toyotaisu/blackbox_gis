def build_data_source(host,port,db,user,password):
    return {
        'host': host,
        'port': port,
        'db': db,
        'user': user,
        'password': password
    }

pg_ne_datasource = build_data_source('localhost',5432,'blackbox','postgres','root')

# Nebraska Configuration
ne_input_folder = r'C:\Users\!krishnaj\work\input'
ne_output_folder =r'C:\Users\!krishnaj\work\output'
ne_boundary_file =r'C:\Users\!krishnaj\work\data\NE_Boundaries\Nebraska_Boundary.shp'

# Iowa Configuration
ia_input_folder = r'C:\Users\!krishnaj\work\input'
ia_output_folder =r'C:\Users\!krishnaj\work\output'
ia_boundary_file =r'C:\Users\!krishnaj\work\data\IA_Boundaries\iowa_border.shp'
# Tolerance in meters
ne_tolerance = '15'


