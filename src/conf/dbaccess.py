import psycopg2

from src.conf.config import pg_ne_datasource as pgnd

db_connections = {}

def get_ne_pgis_db():
    if 'pgis' not in db_connections:
        db_connections['pgis'] = connect_ne_db()
    return db_connections['pgis']

def connect_ne_db():
    print('Connecting to the NE POSTGIS database')
    return psycopg2.connect(host=pgnd['host'],dbname=pgnd['db'],user =pgnd['user'],password=pgnd['password'])