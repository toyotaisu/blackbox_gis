DROP TABLE IF EXISTS blackbox.ia_roads_geom;
CREATE TABLE blackbox.ia_roads_geom AS
SELECT objectid,effective_start_date,effective_end_date,
	geographic_identifier,system_code,route_number,direction,
	ramp_sequence,route_id,
	ST_Transform(geom,4326) as geom,
	ST_Transform(geom,26986) as geom2
FROM blackbox.ia_roads;

DROP INDEX IF EXISTS blackbox.ia_geom_nat_idx;
DROP INDEX IF EXISTS blackbox.ia_geom2_nat_idx;
CREATE INDEX ia_geom_nat_idx ON blackbox.ia_roads_geom USING GIST(geom);
CREATE INDEX ia_geom2_nat_idx ON blackbox.ia_roads_geom USING GIST(geom2);

DROP FUNCTION IF EXISTS blackbox.ia_closest_point_on_line(loc text,tolerance double precision);

CREATE OR REPLACE FUNCTION blackbox.ia_closest_point_on_line(loc text, tolerance double precision) 
RETURNS TABLE(
	distance double precision,
	closest_point text,
	geographicIdentifier character varying(5),
	systemCode character varying(1),
	routeNumber character varying(4),
	_direction character varying(1) ,
	rampSequence character varying(4) ,
	routeId character varying(15)
) AS
$func$
BEGIN
	RETURN QUERY
	with p(point,point2) AS (VALUES (ST_geometryFromText(loc,4326),ST_Transform(ST_geometryFromText(loc,4326),26986)))
	SELECT ST_Distance(iag.geom2,p.point2) distance, 
	ST_AsGeoJSON(ST_ClosestPoint(iag.geom,p.point)) as closest_point,
	geographic_identifier,system_code,route_number,
	direction,ramp_sequence,route_id
	FROM blackbox.ia_roads_geom iag, p 
	WHERE ST_DWithIn(iag.geom2,p.point2,tolerance) 
	order by distance limit 1
	;
END
$func$ LANGUAGE 'plpgsql';

select * from blackbox.ia_closest_point_on_line('POINT(-96.47569 42.5388)',15);
