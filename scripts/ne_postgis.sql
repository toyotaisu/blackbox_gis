-- Table creation
DROP TABLE IF EXISTS blackbox.ne_roads_geom;
CREATE TABLE blackbox.ne_roads_geom  AS 
SELECT *, ST_Transform(geom,26986) as geom2 
FROM blackbox."NE_Roads";

-- Create Index
DROP INDEX IF EXISTS blackbox.ne_geom_nat_idx;
DROP INDEX IF EXISTS blackbox.ne_geom2_nat_idx;
CREATE INDEX ne_geom_nat_idx ON blackbox.ne_roads_geom USING GIST(geom);
CREATE INDEX ne_geom2_nat_idx ON blackbox.ne_roads_geom USING GIST(geom2);

-- Function Creation
DROP FUNCTION IF EXISTS blackbox.ne_closest_point_on_line(loc text,tolerance double precision);
CREATE OR REPLACE FUNCTION blackbox.ne_closest_point_on_line(loc text, tolerance double precision)
RETURNS TABLE (
	distance double precision,
	closest_point text,
	esriSpeedcat smallint,
	esriFuncclass smallint,
	esriPaved character varying(1),
	esriDividerType character varying(1),
    esriIntersectionCategory character varying(1),
    esriLaneCategory smallint,
    esriPhysicalNumLanes smallint,
    esriRoadCl smallint,
    esriKph integer,
    esriRouteType smallint,
	fhwaAadt integer,
    fhwaFSystem smallint,
    fhwaFacilityT smallint,
    fhwaLaneWidth smallint,
    fhwaRouteSign smallint,
    fhwaSpeedLimi smallint,
    fhwaSurfaceTy smallint,
    fhwaThroughLa smallint,
    fhwaUrbanCode integer,
	censusUza smallint
) AS 
$func$
BEGIN
RETURN QUERY
with p(point,point2) AS (VALUES (ST_geometryFromText(loc,4326),ST_Transform(ST_geometryFromText(loc,4326),26986)))
SELECT ST_Distance(neg.geom2,p.point2) distance, 
	ST_AsGeoJSON(ST_ClosestPoint(neg.geom,p.point)) as closest_point,
	esri_speedcat,esri_funcclass,esri_paved,esri_divider_type,
	esri_intersection_category,
	esri_lane_category,esri_physical_num_lanes,
	esri_road_cl,esri_kph esri_kph,esri_route_type,fhwa_aadt,
	fhwa_f_system,fhwa_facility_t,fhwa_lane_width,
	fhwa_route_sign,fhwa_speed_limi,fhwa_surface_ty,
	fhwa_through_la,fhwa_urban_code,census_uza
	FROM blackbox.ne_roads_geom neg, p 
	WHERE ST_DWithIn(neg.geom2,p.point2,tolerance) 
	order by distance limit 1
	;
END
$func$ LANGUAGE 'plpgsql';


--Test
select * from blackbox.ne_closest_point_on_line('POINT(-102.18979789 41.00240004)',15.0);