import json
import logging
import os
import geopandas as gpd
import pandas as pd
from shapely.geometry import Point
from src.conf import config,dbaccess
from multiprocessing import Pool

# Change number of processes according to the CPU coresself.
num_processes = 8

# logging configuration
logger = logging.getLogger('blackbox')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('blackbox_run.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter  =  logging.Formatter('%(asctime)s -%(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)

# Global variables
boundary_shape = gpd.read_file(config.ne_boundary_file)

# Output keys
keys = ['distance','closest_point','esri_speedcat','esri_funcclass','esri_paved','esri_divider_type',
'esri_intersection_category','esri_lane_category','esri_physical_num_lanes','esri_road_cl','esri_kph esri_kph',
'esri_route_type','fhwa_aadt','fhwa_f_system','fhwa_facility_t','fhwa_lane_width','fhwa_route_sign',
'fhwa_speed_limi','fhwa_surface_ty','fhwa_through_la','fhwa_urban_code','census_uza']


def process_file(file):
    '''
    Geospatial analysis for the given file.
    :param file:
    :return:
    '''
    # Get Database connection
    conn = dbaccess.get_ne_pgis_db()
    cur = conn.cursor()

    logger.info('Processing started for file %s.', file)
    file_base_name = os.path.splitext(os.path.basename(file))[0]
    df = pd.read_csv(os.path.join(config.ne_input_folder, file))
    df['gps_lat'].fillna(0, inplace=True)
    df['gps_long'].fillna(0, inplace=True)
    df['is_ne'] = df[['gps_long', 'gps_lat']].apply(Point, axis=1).apply(lambda p: boundary_shape.contains(p))

    # Group files based on the unique drive (combination of subj and drive)
    dfg = df.groupby(['subj', 'drive'])

    output_file = os.path.join(config.ne_output_folder, file_base_name + '_processed.csv')

    for key, group in dfg:
        logger.info('File %s: Spatial analysis for group %s', file, key)
        basedf = group[["subj", "file", "drive", "time_utc", "gps_lat", "gps_long", "gps_heading", "is_ne"]]
        result_arr = []
        total = group['is_ne'].sum()
        print('total ', total)
        if total > 1:
            logger.info('File %s: Group %s:  Route is in Nebraska.', file,key)
            for id, row in group.iterrows():
                cur.callproc('blackbox.ne_closest_point_on_line',
                             ('POINT({} {})'.format(row['gps_long'], row['gps_lat']),
                              config.ne_tolerance))
                records = cur.fetchall()
                if len(records) > 0:
                    # logging.info('Resolved the nearest route ')
                    # The database function returns at most one record
                    record = dict(zip(keys, records[0]))
                    coordinates = json.loads(record['closest_point'])['coordinates']
                    # Remove key from the dictionary
                    record.pop('closest_point', None)
                    record['route_long'] = coordinates[0]
                    record['route_lat'] = coordinates[1]
                    record['route_50ft'] = 1
                    record['route_success'] = 1
                    result_arr.append(record)
                else:
                    # logging.info('No nearest route exist.')
                    record = {k: '' for k in keys if k != 'closest_point'}
                    record['distance'] = -1
                    record['route_long'] = ''
                    record['route_lat'] = ''
                    record['route_50ft'] = 0
                    record['route_success'] = 1
                    result_arr.append(record)
        else:
            logger.info('Route is not in Nebraska')
            for id, row in group.iterrows():
                record = {k: '' for k in keys if k != 'closest_point'}
                record['distance'] = ''
                record['route_long'] = ''
                record['route_lat'] = ''
                record['route_50ft'] = 0
                record['route_success'] = 0
                result_arr.append(record)

        resultdf = pd.DataFrame(result_arr, index=basedf.index)
        finaldf = basedf.join(resultdf)
        finaldf.to_csv(output_file, index=False, mode='a', header=(not os.path.exists(output_file)))
        logger.info('Finished processing for %s', file)





def main():
    files = [file for file in os.listdir(config.ne_input_folder) if file.endswith('.csv')]

    with Pool(num_processes) as pool:
        pool.map(process_file,files)

    logger.info('Completed processing for all files in folder %s', config.ne_input_folder)



# Run the main function

if __name__ == '__main__':
    main()
